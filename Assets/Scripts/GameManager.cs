﻿using UnityEngine;

public enum GameState { NullState, MainMenu, Game }
public delegate void OnStateChangeHandler();

public class GameManager
{ 
    protected GameManager() { }
    private static GameManager _instance = null;


    public event OnStateChangeHandler OnStateChange;   
    public GameState gameState { get; private set; }

    // Singleton pattern implementation
    public static GameManager Instance
    {
        get
        {
            if (GameManager._instance == null)
            {
                GameManager._instance = new GameManager();
            }
            return GameManager._instance;
        }
    }

    public void SetGameState(GameState gameState)
    {
        this.gameState = gameState;
        if (OnStateChange != null)
        {
            OnStateChange();
        }
    }


}