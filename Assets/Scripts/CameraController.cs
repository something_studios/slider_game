﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    //Game manager para indicar el estado del juego
    private GameManager GM;

    void Awake()
    {
        //Se le pasa el valor de referencia....
        GM = GameManager.Instance;
        //.... y se le pasa un evento que uno puede crear
        GM.OnStateChange += HandleOnStateChange;

        //Se le dice el estado del juego, que por el momento es Game
        GM.SetGameState(GameState.Game);
    }


    public GameObject player;
    private Vector3 offset;

    // Use this for initialization
    void Start()
    {
        offset = transform.position - player.transform.position;        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = player.transform.position + offset;
    }

    void HandleOnStateChange()
    {
        Debug.Log("Handling state change to: " + GM.gameState);
    }

}
