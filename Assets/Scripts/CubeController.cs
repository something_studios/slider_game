﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class CubeController : MonoBehaviour {

    //Variable para saber que tipo de personaje es el jugador
    public enum playerType { skier, goat }

    public float speed;
    public Camera cam;
    private Rigidbody rb;
    private float increment;
    private RaycastHit rayPosition;
    public float maxVelocity;
    private float thrust;
    private Vector3 playerScale;  

    private GameManager GM;

    void Awake()
    {
        //Se le pasa el valor de referencia....
        GM = GameManager.Instance;
        //.... y se le pasa un evento que uno puede crear
        GM.OnStateChange += HandleOnStateChange;

        //Se le dice el estado del juego, que por el momento es Game
        GM.SetGameState(GameState.Game);
    }


    void HandleOnStateChange()
    {
        //Debug.Log("Handling state change to: " + GM.gameState);
    }


    private void shrinking()
    {
        if (transform.localScale.x >= 1.3f && transform.localScale.y >= 1.3f && transform.localScale.z >= 1.3f)
            transform.localScale -= new Vector3(0.2f, 0.2f, 0); 
    }
    

    private void touchInput(playerType player)
    {
            


        if (Input.touchSupported && Input.touchCount == 1)
        {            
            switch (Input.GetTouch(0).phase)
            {

                case TouchPhase.Moved:
                    if (player == playerType.goat)
                    {
                        
                        if (Input.GetTouch(0).deltaPosition.x > 0)
                        {
                            //Agregar limite para mover el cubo, poner limite entre la pantalla y distancia a mover
                            rb.AddRelativeForce(new Vector3(1,1,0) * thrust);
                            
                        }

                    }

                    break;


                case TouchPhase.Stationary:
                    // Construct a ray from the current touch coordinates
                    /*ray = cam.ScreenPointToRay(Input.GetTouch(0).position);

                    Physics.Raycast(ray, out rayPosition);
                    if (rayPosition.collider.tag == "Player" && transform.localScale.x > 0.5f && transform.localScale.y > 0.5f)
                        rayPosition.collider.transform.localScale -= new Vector3(0.2f, 0.2f, 0);*/
                    /*if (transform.localScale.x >= 0.3f && transform.localScale.y >= 0.3f && transform.localScale.z >= 0.3f)
                        transform.localScale -= new Vector3(0.2f, 0.2f, 0);                    */
                                            
                       // playerState.Invoke();
                    if (player == playerType.skier)
                        shrinking();

                    break;



                case TouchPhase.Ended:

                  
                    if (transform.localScale != playerScale)
                        transform.localScale = playerScale;
                    
                    
                    break;

                case TouchPhase.Canceled:

                    if (transform.localScale != playerScale)
                        transform.localScale = playerScale;

                    break;
            }




        }
    }    

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        maxVelocity = 6.5f;
        increment = 0.0f;
        speed = 2.5f;
        if (cam == null)
        {
            cam = Camera.main;

        }
        
        playerScale = transform.localScale;

        rb.AddForce(Physics.gravity);

	}
	
    //Se usa cuando hay un RigidBody
    void FixedUpdate()
    {
        
        increment += 0.03f;

        //Para probar como se desliza el cubo, le di una fuerza de empuje
            
        rb.AddForce(Mathf.Clamp(0.5f * speed + increment, 0, maxVelocity), 0, 0);
                
        touchInput(playerType.goat);        
                
    }
    

  
    
  

}



